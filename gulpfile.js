var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var inject = require('gulp-inject');
var clean = require('gulp-clean');
var connect = require('gulp-connect');
var historyApiFallback = require('connect-history-api-fallback');
var templateCache = require('gulp-angular-templatecache');

var config = require('./gulpconfig.json');
var dist = config.dist.path;

gulp.task('clean', function() {
    return gulp.src(config.clean.src, {force: true, read: false})
        .pipe(clean());
});

gulp.task('vendor', function() {
    return gulp.src(config.vendor.src)
        .pipe(concat(config.vendor.concat))
        .pipe(gulp.dest(dist));
});

gulp.task('vendor-sourcemaps-js', function() {
    gulp.src(config.vendorSourcemapsJs.src)
        .pipe(gulp.dest(config.vendorSourcemapsJs.dist));
});

gulp.task('vendor-sourcemaps', function() {
    gulp.start('vendor-sourcemaps-js');
});

gulp.task('js', function () {
    return gulp.src(config.js.src)
        .pipe(sourcemaps.init())
        .pipe(concat(config.js.concat))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dist))
        .pipe(connect.reload());
});

gulp.task('css', function() {
    return gulp.src(config.css.src)
        .pipe(minifyCss())
        .pipe(concat(config.css.concat))
        .pipe(gulp.dest(dist))
        .pipe(connect.reload());
});

gulp.task('fonts', function() {
    gulp.src(config.fonts.src)
        .pipe(gulp.dest(config.fonts.dist));
});

gulp.task('images', function() {
    gulp.src(config.images.src)
        .pipe(gulp.dest(config.images.dist));
});

gulp.task('index-build', ['css', 'js', 'vendor'], function() {
    gulp.start('index');
});

gulp.task('index', function() {
    gulp.src(config.index.src)
        .pipe(inject(gulp.src(config.index.inject.src, {read: false}), {ignorePath: config.index.inject.ignorePath}))
        .pipe(gulp.dest(dist))
        .pipe(connect.reload());
});

gulp.task('templates', function() {
    gulp.src(config.templates.src)
        .pipe(templateCache(config.templates.filename, {
            standalone: config.templates.standalone
        }))
        .pipe(gulp.dest(dist))
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.start('build');

    gulp.watch(config.watch.src.js, ['js']);
    gulp.watch(config.watch.src.css, ['css']);
    gulp.watch(config.watch.src.index, ['index']);
    gulp.watch(config.watch.src.templates, ['templates']);
});

gulp.task('connect', function () {
    connect.server({
        root: config.connect.server.root,
        port: config.connect.server.port,
        host: config.connect.server.host,
        livereload: true,
        middleware: function(connect, opt) {
            return [ historyApiFallback ];
        }
    })
});

gulp.task('build', ['vendor', 'vendor-sourcemaps', 'js', 'css', 'fonts', 'images', 'index-build', 'templates'], function () {

});

gulp.task('default', ['clean'], function() {
    gulp.start('build');
});

