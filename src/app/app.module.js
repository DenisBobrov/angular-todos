(function() {
    angular
        .module('app', [
            'app.core',
            'app.todos',
        ])
        .constant('_', _) // lodash global var
        .run(function($rootScope, $route) {
            // helper for generating url's
            $rootScope.path = function(routeName, routeParams) {

                // Iterate over all available routes
                for (var path in $route.routes) {
                    // First match the controller
                    if ($route.routes[path].routeName != routeName){
                        continue;
                    }

                    // Then match the params
                    var paramsMatched = true;
                    var result = path;
                    for (var i = 0; i < $route.routes[path].keys.length; i++) {
                        var key = $route.routes[path].keys[i];
                        if (!(key.name in routeParams) && !key.optional) {
                            paramsMatched = false;
                            break;
                        }

                        // Construct the path with given parameters in it
                        result = result.replace(new RegExp(':' + key.name + '[?*]?'), function() {
                            return key.name in routeParams ? routeParams[key.name] : '';
                        });
                    }
                    if (!paramsMatched) continue;

                    return result;
                }

                // No such controller in route definitions
                return undefined;
            };
        });
})();
