(function() {
    angular
        .module('app.core')
        .controller('NavController', NavController);

    function NavController($route) {
        var vm = this;
        
        vm.$route = $route;
    }
})();