(function() {
    angular
        .module('app.core', [
            'templates',
            'ngRoute',
            'ui.bootstrap',
            'restangular',
        ])
        .config(coreConfig);

    function coreConfig($locationProvider, $routeProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                redirectTo: '/home'
            })
            .otherwise({
                controller: 'Error404Controller',
                controllerAs: 'vm',
                templateUrl: 'core/views/404.html'
            });
    }
})();