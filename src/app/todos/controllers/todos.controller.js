(function() {
    angular
        .module('app.todos')
        .controller('TodosController', TodosController);


    function TodosController($log, $filter, todosService) {
        var vm = this;

        vm.todos = [];
        vm.newTodo = '';
        vm.remainingCount = 0;

        // actions
        vm.addTodo = addTodo;

        init();

        function addTodo() {
            var title = vm.newTodo;

            if (!title.trim()) {
                return;
            }

            var todo = {
                title: title,
                isCompleted: false
            };

            todosService.createTodo(todo);

            vm.newTodo = '';

            addTodos([todo]);
        }

        function addTodos(todos) {
            vm.todos = vm.todos.concat(todos);
            vm.remainingCount = $filter('filter')(vm.todos, {isCompleted: false}).length;
        }

        function init() {
            var todos = todosService.findTodos();

            addTodos(todos);
        }
    }
})();