(function() {
    angular
        .module('app.todos')
        .factory('todosService', TodosService);

    function TodosService($window, $log, _) {
        var TODOS_STORAGE_ID = 'todos';

        return {
            createTodo: createTodo,
            findTodos: findTodos
        };

        function createTodo(todo) {
            var todosInStorage = findTodos();
            var lastInsertedId = _.last(todosInStorage);

            todo.id = lastInsertedId;

            $log.debug('Create new todo');
            $log.debug(todo);

            // saving
            todosInStorage.push(todo);

            saveTodosInStorage(todosInStorage);

            return todo;
        }

        function saveTodosInStorage(todos) {
            $window.localStorage.setItem(TODOS_STORAGE_ID, JSON.stringify(todos));
        }

        function findTodos() {
            var storage = $window.localStorage;

            return JSON.parse(storage.getItem(TODOS_STORAGE_ID) || '[]');
        }
    }
})();