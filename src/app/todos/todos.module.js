(function() {
    angular
        .module('app.todos', [])
        .config(function ($locationProvider, $routeProvider) {
            $locationProvider.html5Mode(true);

            $routeProvider
                .when('/todos', {
                    controller: 'TodosController',
                    controllerAs: 'vm',
                    templateUrl: 'todos/views/todos.html'
                })
                .otherwise({
                    redirectTo: '/todos'
                });
        });
})();

